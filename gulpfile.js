var gulp           = require('gulp'),
    sass           = require('gulp-sass'),
    concat         = require('gulp-concat'),
    browserSync    = require('browser-sync').create(),
    autoprefixer   = require('gulp-autoprefixer'),
    sourcemaps     = require('gulp-sourcemaps'),
    twig           = require('gulp-twig'),

    src = {
      scss:     'dev/scss/**/*.scss',
      js:       'dev/js/*.js',
      twig:     'dev/twig/*.html',
      twigs:    'dev/twig/**/*.twig',
      html:     'html/*.html',
      css:      'html/css',
      map:      '/maps',
      scripts:  'html/js'
    };

// Compile Twig templates to HTML
gulp.task('twig', function() {
  return gulp.src(src.twig) // run the Twig template parser on all .html files in the "src" directory
    .pipe(twig())
    .pipe(gulp.dest('./')); // output the rendered HTML files to the "dist" directory
});

// Static Server + watching scss/html files
gulp.task('serve', ['sass', 'pack-js', 'twig'], function() {

  browserSync.init({
    server: {
      baseDir: "./",
    }
  });

  gulp.watch(src.scss, ['sass']);
  gulp.watch(src.js, ['pack-js']);
  gulp.watch([src.twig, src.twigs], ['twig']);
  gulp.watch(src.html).on('change', browserSync.reload);
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src(src.scss)
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .on('error', function (err) {
      console.log('Hey error! ' + err);
    })
    .pipe(autoprefixer({browsers: ['last 4 versions']}))
    .pipe(sourcemaps.write(src.map))

    .pipe(gulp.dest(src.css))
    .pipe(browserSync.stream());
});

gulp.task('pack-js', function () {
  return gulp.src(src.js)
    .pipe(concat('script.js'))
    .pipe(gulp.dest(src.scripts));
});

gulp.task('default', ['serve']);
